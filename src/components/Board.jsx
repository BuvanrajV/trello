import React, { Component } from "react";
import { Link } from "react-router-dom";

class Board extends Component {
  render() {
    return (
      <Link to={`/${this.props.board.id}`}>
        <div
          className="card boards-div"
          style={{
            backgroundImage: `url(${this.props.board.prefs.backgroundImage})`,
            backgroundColor: `${this.props.board.prefs.backgroundColor}`,
          }}
        >
          <div className="card-body">
            <h5 className="card-title">{this.props.board.name}</h5>
          </div>
        </div>
        
      </Link>
    );
  }
}

export default Board;
