import { Link } from "react-router-dom";

export default function Home() {
  return (
    <div>
      <div className="home-container">
        <div className="home-welcome">Welcome To Trellos</div>
        <Link to="/boards">
          <button type="button" className="btn btn-primary">
            Boards
          </button>
        </Link>
      </div>
    </div>
  );
}
