import axios from "axios";

const key = "5e8275b6c54749d6f735754fcdca1d46";
const token =
  "ATTA3570eb83b176658016190b945d66699e5ab4e4382a81c5d3617e24b9ebe6bba23848367F";

export function getBoards() {
  return axios.get(
    `https://api.trello.com/1/members/buvanrajvaradharajan/boards?key=${key}&token=${token}`
  );
}

export function postBoard(boardTitle) {
  return axios.post(
    `https://api.trello.com/1/boards/?name=${boardTitle}&key=${key}&token=${token}`
  );
}

export function getLists(id) {
  return axios.get(
    `https://api.trello.com/1/boards/${id}/lists?key=${key}&token=${token}`
  );
}

export function postList(id, listTitle) {
  return axios.post(
    `https://api.trello.com/1/boards/${id}/lists?name=${listTitle}&key=${key}&token=${token}`
  );
}

export function deleteListByAxios(id) {
  return axios.put(
    `https://api.trello.com/1/lists/${id}/?closed=${true}&key=${key}&token=${token}`
  );
}

export function getCards(id){
    return axios
    .get(
      `https://api.trello.com/1/lists/${id}/cards?key=${key}&token=${token}`
    )
}

export function postCard(id,newCard){
    return axios
    .post(
      `https://api.trello.com/1/cards?idList=${id}&name=${newCard}&key=${key}&token=${token}`
    )
}

export function deleteCardByAxios(id){
    return axios.delete(
        `https://api.trello.com/1/cards/${id}?key=${key}&token=${token}`
      );
}