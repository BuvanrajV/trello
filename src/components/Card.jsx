import React, { Component } from "react";

class Class extends Component {
  render() {
    return (
      <div key={this.props.id}>
        {this.props.card.name}
        <button
          onClick={() => this.props.onDelete(this.props.card.id)}
          className="button-x"
        >
          delete
        </button>
      </div>
    );
  }
}

export default Class;
