import { Link } from "react-router-dom";

export default function Navbar(){
  return (
    <nav className="navbar">
      <Link to={`/`}>
        <div className="fw-bold" style={{color:"white"}}>Trello</div>
        </Link>
    </nav>
  );
}






