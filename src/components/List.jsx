import React, { Component } from "react";
import Cards from "./Cards";

class List extends Component {
  state = {
    addList: false,
    newListName: "",
  };

  render() {
    return (
      <div className="d-flex flex-column p-1  list-div" key={this.props.id}>
        <div> {this.props.list.name}</div>
        <Cards id={this.props.id} onDelete={this.props.onDelete}></Cards>
      </div>
    );
  }
}

export default List;
