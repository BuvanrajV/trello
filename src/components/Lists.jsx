import React, { Component } from "react";
import { deleteListByAxios, getLists, postList } from "./api";
import List from "./List";

class Lists extends Component {
  state = {
    lists: [],
    addList: false,
    newListName: "",
    isLoading: true,
  };

  componentDidMount() {
    getLists(this.props.match.params.id)
      .then((data) => {
        this.setState({ lists: data.data, isLoading: false });
      })
      .catch((err) => console.error(err));
  }

  updateLists = (event) => {
    event.preventDefault();
    postList(this.props.match.params.id, this.state.newListName).then(
      (response) =>
        this.setState({
          lists: [...this.state.lists, response.data],
          addList: false,
        })
    );
  };

  deleteList = (id) => {
    deleteListByAxios(id).then(() =>
      this.setState({
        lists: this.state.lists.filter((list) => list.id !== id),
      })
    );
  };

  render() {
    return this.state.isLoading ? (
      <div className="loading">
        <div className="spinner-border" role="status">
          <span className="sr-only"></span>
        </div>
      </div>
    ) : (
      <div className="d-flex p-5 lists-container">
        {this.state.lists.map((list) => {
          return (
            <List
              id={list.id}
              key={list.id}
              list={list}
              onDelete={this.deleteList}
            />
          );
        })}
        <div>
          <button
            type="button"
            className="btn btn-primary"
            onClick={() => {
              this.setState({ addList: true });
            }}
          >
            Add Another List
          </button>
          {this.state.addList && (
            <form onSubmit={this.updateLists}>
              <input
                type="text"
                className="form-control"
                placeholder="List Name "
                onChange={(e) => {
                  this.setState({ newListName: e.target.value });
                }}
              ></input>
              <button type="submit">Add List</button>
              <span
                onClick={() => {
                  this.setState({ addList: false });
                }}
                className="span-x"
              >
                X
              </span>
            </form>
          )}
        </div>
      </div>
    );
  }
}

export default Lists;
