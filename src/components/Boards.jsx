import React, { Component } from "react";
import Board from "./Board";
import { getBoards, postBoard } from "./api";

class Boards extends Component {
  state = {
    boards: [],
    boardName: "",
    isPopup: false,
    isLoading: true,
  };

  componentDidMount() {
    getBoards()
      .then((boardsInfo) =>
        this.setState({
          boards: boardsInfo.data,
          isLoading: false,
        })
      )
      .catch((err) => console.error(err));
  }

  updateBoards = (event) => {
    event.preventDefault();
    postBoard(this.state.boardName)
      .then((response) =>
        this.setState({
          boards: [...this.state.boards, response.data],
          isPopup: false,
        })
      )
      .catch((err) => console.error(err));
  };

  loadingForBoards = () => {
    this.setState({ isLoadingForBoard: false });
  };

  render() {
    return (
      <div className="container boards-container">
        {this.state.isPopup && (
          <div className="popup-div">
            <h4>Create Board</h4>
            <form onSubmit={this.updateBoards}>
              <input
                type="text"
                className="form-control"
                placeholder="Board Title "
                onChange={(e) => {
                  this.setState({ boardName: e.target.value });
                }}
              ></input>
              <button type="submit" className="btn btn-primary">
                Create
              </button>
              <button
                type="button"
                className=" btn btn-danger"
                onClick={() => {
                  this.setState({ isPopup: false });
                }}
              >
                Close
              </button>
            </form>
          </div>
        )}
        {this.state.isLoading ? (
          <div className="loading">
            <div className="spinner-border" role="status">
              <span className="sr-only"></span>
            </div>
          </div>
        ) : (
          <>
            <div
              className="card boards-div"
              onClick={() => {
                this.setState({ isPopup: true });
              }}
            >
              <div className="card-body">
                <h5 className="card-title" style={{ color: "black" }}>
                  Create New Board
                </h5>
              </div>
            </div>
            {this.state.boards.map((board) => (
              <Board board={board} key={board.id}></Board>
            ))}
          </>
        )}
      </div>
    );
  }
}
export default Boards;
