import React from "react";
import { BrowserRouter, Route, Switch } from "react-router-dom";
import Navbar from "./components/Navbar";
import Home from "./components/Home";
import Boards from "./components/Boards";
import Lists from "./components/Lists";

function App (){
    return (
        <BrowserRouter>
          <Navbar />
          <Switch>
            <Route exact path='/' component={Home} />
            <Route exact path="/boards" component={Boards} />
            <Route exact path="/:id" component={Lists} />
          </Switch>
        </BrowserRouter>
    );
}

export default App;