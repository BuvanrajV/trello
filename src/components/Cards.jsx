import React, { Component } from "react";
import Card from "./Card";
import { deleteCardByAxios, getCards, postCard } from "./api";

class Cards extends Component {
  state = {
    cards: [],
    addCard: false,
    newCardName: "",
    isLoading:true,
  };

  componentDidMount() {
    getCards(this.props.id)
      .then((data) => this.setState({ cards: data.data ,isLoading:false}))
      .catch((err) => console.error(err));
  }

  updateCards = () => {
    postCard(this.props.id, this.state.newCardName)
      .then((response) =>
        this.setState({
          cards: [...this.state.cards, response.data],
          addCard: false,
        })
      )
      .catch((err) => console.error(err));
  };

  deleteCard = (id) => {
    deleteCardByAxios(id).then(() =>
      this.setState({
        cards: this.state.cards.filter((card) => card.id !== id),
      })
    );
  };

  render() {
    return (
      this.state.isLoading?(
        <div>
        <div className="spinner-border" role="status">
          <span className="sr-only"></span>
        </div>
      </div>
      ):(
        <>
        {this.state.cards.map((card) => (
          <Card
            card={card}
            id={card.id}
            key={card.id}
            onDelete={this.deleteCard}
          ></Card>
        ))}
        {this.state.addCard ? (
          <div>
            <div className="input-class">
              <input
                type="text"
                className="form-control"
                placeholder="Card Name"
                onChange={(e) => {
                  this.setState({ newCardName: e.target.value });
                }}
              ></input>
            </div>
            <div>
              <button
                type="button"
                onClick={() => {
                  this.updateCards();
                }}
              >
                Add Card
              </button>{" "}
              <button
                onClick={() => {
                  this.setState({ addCard: false });
                }}
                className="button-x"
              >
                Close
              </button>
            </div>
          </div>
        ) : (
          <div>
            <div
              onClick={() => {
                this.setState({ addCard: true });
              }}
              className="add-card "
              style={{cursor:"pointer"}}
            >
              + Add Card
            </div>
            <button
              type="button"
              className="btn btn-danger"
              onClick={() => this.props.onDelete(this.props.id)}
            >
              delete
            </button>
          </div>
        )}
      </>
      )
      
    );
  }
}

export default Cards;
